
/*                                In the name of Allah
 * =====================================================================================
 *
 *       Filename:  GeneralRosNode.h
 *
 *    Description: This file contains a ROS node template that has the ability to
 *                 perform both as a subscriber and publisher
 *
 *        Version:  1.0
 *        Created:  13/12/2022
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Abdollah Ebadi , info@boundless-systems.com.au
 *   Organization:  Boundless-Systems PTY LTD
 *
 * =====================================================================================
 */


#ifndef _GENERAL_ROS_NODE_
#define _GENERAL_ROS_NODE_

#include <ros/ros.h>
#include <iostream>
#include <functional>
#include <std_msgs/String.h>
#include <memory>



enum class NodeType {
    PUB_ONLY,
    SUB_ONLY,
    PUB_SUB,
    DEFAULT
} ;



template <typename PUBLISHER_TOPIC_TYPE, typename SUBSCRIBER_TOPIC_TYPE>
class GeneralRosNode
{
private:

    NodeType nodeType = NodeType::DEFAULT ;

    std::string nodeName = "" ;

    // Note : the node needs to be initialized in the driver program prior to
    // using this node template
    ros::NodeHandle _n ;

    //---------------------------
    std::string publisherTopic ;
    uint32_t publisherQueueSize ;
    ros::Publisher _pub ;
    bool isPublisherEnabled = false ;

    //---------------------------
    std::string subscriberTopic ;
    uint32_t subscriberQueueSize ;
    ros::Subscriber _sub ;
    boost::function<void(const boost::shared_ptr<SUBSCRIBER_TOPIC_TYPE const> &)> callback ;
    bool isCallbackProvidedForSubscriber = false ;
    bool isSubscriberEnabled = false ;


public:
    GeneralRosNode (
        std::string publisher_topic,
        uint32_t publisher_queue_size,
        std::string subscriber_topic,
        uint32_t subscriber_queue_size,
        std::string name
    )
    {
        publisherTopic = publisher_topic ;
        publisherQueueSize = publisher_queue_size ;
        subscriberTopic = subscriber_topic ;
        subscriberQueueSize = subscriber_queue_size ;

        nodeType = NodeType::PUB_SUB ;
        nodeName = name ;
    } ;

    GeneralRosNode ( std::string name ) : nodeName ( name ) {  } ;

    void setPublisherParams (
        std::string publisher_topic,
        uint32_t publisher_queue_size )
    {

        publisherTopic = publisher_topic ;
        publisherQueueSize = publisher_queue_size ;

        if ( nodeType == NodeType::SUB_ONLY )
            nodeType = NodeType::PUB_SUB ;
        else
            nodeType = NodeType::PUB_ONLY ;

    } ;

    void setSubscriberParams (
        std::string subscriber_topic,
        uint32_t subscriber_queue_size )
    {

        subscriberTopic = subscriber_topic ;
        subscriberQueueSize = subscriber_queue_size ;

        if ( nodeType == NodeType::PUB_ONLY )
            nodeType = NodeType::PUB_SUB ;
        else
            nodeType = NodeType::SUB_ONLY ;

    } ;


    void disablePublisherFeature()
    {
        if ( isPublisherEnabled ) {
            _pub.shutdown();
            isPublisherEnabled = false ;
        }

    };


    void disableSubscriberFeature()
    {
        if ( isSubscriberEnabled ) {
            _sub.shutdown();
            isSubscriberEnabled = false ;
        }
    };

    void setSubscriptionCallback ( std::function<void(const boost::shared_ptr<SUBSCRIBER_TOPIC_TYPE const> &)> callback )
    {
        this->callback = callback ;
        isCallbackProvidedForSubscriber = true ;
    } ;

    void advertisePublisher()
    {
        if ( !isPublisherEnabled ) {
            _pub = _n.advertise<PUBLISHER_TOPIC_TYPE> ( publisherTopic, publisherQueueSize ) ;
            isPublisherEnabled = true ;
        } else {
            std::string msg ( nodeName + " - Publisher feature already enabled for node..." );
            ROS_INFO ( "%s", msg.c_str() ) ;
        }
    } ;

    void startSubscriber()
    {
        if ( isCallbackProvidedForSubscriber ) {
            if ( !isSubscriberEnabled ) {
                _sub = _n.subscribe<SUBSCRIBER_TOPIC_TYPE>(subscriberTopic, subscriberQueueSize ,  callback) ;
            } else {
                std::string msg ( nodeName + " - Subscriber feature already enabled for node..." ) ;
                ROS_INFO ( "%s", msg.c_str() ) ;
            }
        } else {
            std::string msg ( nodeName + " - No callback provided for subscriber, cannot start subscriber..!" ) ;
            ROS_INFO ( "%s", msg.c_str() ) ;
        }
    } ;

    void publish ( PUBLISHER_TOPIC_TYPE &msg )
    {
        _pub.publish ( msg );
    }
} ;


#endif
